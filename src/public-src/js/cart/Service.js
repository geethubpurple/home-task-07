define([
    'backbone',
    'backbone.marionette'
], function (Backbone, Marionette) {
    var Service = Marionette.Object.extend({
        initialize: function () {
            if (!localStorage.getItem('cart')) {
                this._data = {};
            } else {
                try {
                    this._data = JSON.parse(localStorage.getItem('cart'));
                    if (typeof(this._data) !== 'object') {
                        this._data = {};
                    }
                } catch (e) {
                    this._data = {};
                }
            }

            this.model = new Backbone.Model({
                count: this.count()
            });
        },

        _save: function () {
            localStorage.setItem('cart', this.serialize());
            this.model.set('count', this.count());
        },

        serialize: function () {
            return JSON.stringify(this._data);
        },

        add: function (id, quantity) {
            if (quantity === 0) {
                this.remove(id);

            } else {
                this._data[id] = quantity || 1;
                this._save();
            }
        },

        remove: function (id) {
            delete this._data[id];
            this._save();
        },

        toggle: function (id) {
            if (this.has(id)) {
                this.remove(id);
            } else {
                this.add(id);
            }
        },

        count: function () {
            return Object.keys(this._data).length;
        },

        has: function (id) {
            return this._data.hasOwnProperty(id);
        },
        removeAll: function () {
            this._data = {};
            this._save();
        }
    });

    return new Service;
});