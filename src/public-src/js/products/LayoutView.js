define([
    'backbone.marionette',
    './template'
], function (Backbone, template) {
    return Backbone.LayoutView.extend({
        template: template,

        regions: {
            cart: '.cart-strip',
            products: '.products'
        }
    });
});